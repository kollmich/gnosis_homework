import requests
import pandas as pd

def get_transact(address):
    url = "https://safe-transaction-mainnet.safe.global/api/v1/safes/{0}/multisig-transactions/".format(address)

    s = requests.Session()

    df = pd.DataFrame()

    i = 0

    while True:
        querystring = {
                    "limit": 200,
                    "offset": i*200,
                    }
        try:
            response = s.request("GET", url, params = querystring)
            response.raise_for_status()
        except requests.exceptions.HTTPError as errh:
            print ("Http Error:",errh)
        except requests.exceptions.ConnectionError as errc:
            print ("Error Connecting:",errc)
        except requests.exceptions.Timeout as errt:
            print ("Timeout Error:",errt)
        except requests.exceptions.RequestException as err:
            print ("OOps: Something Else",err)

        temp_df = pd.json_normalize(response.json())
        temp_df = pd.DataFrame(temp_df['results'][0])

        if len(temp_df.index) == 0:
            break

        df = df.append(temp_df, ignore_index=True)
        i += 1

    origin_count = df['origin'].str.contains('WalletConnect').value_counts()
    print(origin_count)
    
get_transact('0xBbA4C8eB57DF16c4CfAbe4e9A3Ab697A3e0C65D8')